package org.nephtys.cqrssync

import java.util.concurrent.atomic.{AtomicBoolean, AtomicLong}

import org.nephtys.cqrssync.datastructures._
import org.nephtys.cqrssync.services.{HttpServiceComponent, IndexedDBServiceComponent, InteractionServiceComponent, LocalStorageServiceComponent}
import rxscalajs.Observable
import upickle.default._

import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by nephtys on 11/25/16.
  */
trait SyncModule[Aggregate <: AggregateWithID, Commandable <: Command[Aggregate, Eventable], Eventable <: Event[Aggregate]] {

  this: HttpServiceComponent with IndexedDBServiceComponent with InteractionServiceComponent with LocalStorageServiceComponent =>


  //TODO: implement those functions locally to correctly execute the framework task of synchronizing

  def testCommand(command : Commandable)(implicit reader1 : Reader[Aggregate], reader2 : Reader[Commandable], writer1 : Writer[Aggregate], writer2 : Writer[Commandable], reader3 : Reader[Eventable]) : Future[Boolean] = ???


  //TODO: maybe trigger http request via debounce mechanic? could work great, but hard to test
  def testAndApplyCommand(command : Commandable)(implicit reader1 : Reader[Aggregate], reader2 : Reader[Commandable], writer1 : Writer[Aggregate], writer2 : Writer[Commandable], reader3 : Reader[Eventable]) : Future[Try[Eventable]]  = ???

  /**
    * will check for local commands and attempt to push them
    * (removing them locally too), and in any case pull a fresh GET.
    *
    * @return true if GET was successful and eTag changed (so actual fresh remote content downloaded!)
    */
  def triggerTimer()(implicit reader1 : Reader[Aggregate], reader2 : Reader[Commandable], writer1 : Writer[Aggregate], writer2 : Writer[Commandable], reader3 : Reader[Eventable]) : Future[Boolean]  = {
    val commands = getCommandsYetToSend
    val promise : Promise[Boolean] = Promise[Boolean]()
    if (commands.nonEmpty && !currentlySendingCommands.get()) {
      currentlySendingCommands.set(true)
      val json : String = write(commands)
      //TODO: trigger rx if needed
      this.httpService.postTo(urlForPost, json).flatMap(resp => {
        if (resp.status == HttpServiceReturnValues.SUCCESS) {
          processRemotePostResult(read[Seq[Failable[Eventable]]](resp.body).map(_.asTry))
        }
        this.httpService.getFrom(urlForGet, currentETag.get())}).onComplete {
        case Success((resp : HttpServiceReturnValues.CachedReturnValue)) => {
          val newValues = resp.status == HttpServiceReturnValues.SUCCESS && resp.eTag > currentETag.get()
          if(newValues) {
            processRemoteGetResult(read[Seq[Aggregate]](resp.body))
          }
          promise.trySuccess(newValues)
        }
        case Failure(e) => {
          println(s"http failed with error $e")
          promise.trySuccess(false)
        }
      }
    } else {
      this.httpService.getFrom(urlForGet, currentETag.get()).onComplete {
      case Success(resp) => {
        val newValues = resp.status == HttpServiceReturnValues.SUCCESS && resp.eTag > currentETag.get()
        if(newValues) {
          processRemoteGetResult(read[Seq[Aggregate]](resp.body))
        }
        promise.trySuccess(newValues)
      }
      case Failure(e) => {
        println(s"http failed with error $e")
        promise.trySuccess(false)
      }
    }
    }
    promise.future
  }

  def processRemoteGetResult(seq : Seq[Aggregate]) = ???
  def processRemotePostResult(seq : Seq[Try[Event[Aggregate]]]) = ???

  protected val urlForGet : String = "/aggregate"
  protected val urlForPost : String = "/commands"

  private val currentlySendingCommands = new AtomicBoolean(false)
  protected def getCommandsYetToSend : Seq[Commandable] = ???

  private val currentETag = new AtomicLong(-1)

  def setToken(newtoken : Option[String])(implicit reader1 : Reader[Aggregate], reader2 : Reader[Commandable], writer1 : Writer[Aggregate], writer2 : Writer[Commandable], reader3 : Reader[Eventable]) : Future[Option[Email]]  = {
    this.localStorageService.writeTo(this.TokenLocalStorageKey, newtoken.getOrElse(""))
    //TODO: trigger observables
    tokenInMemory = newtoken.filter(_.nonEmpty)
    val email : Option[Email] = tokenInMemory.flatMap(s => Try(Email(OpenIDToken.base64EncodedTokenWithBearerHeader(s).email)).toOption)
    triggerTimer().map(b => email)
  }

  private var tokenInMemory : Option[String] = Some(this.localStorageService.readFrom(this.TokenLocalStorageKey)).filter(s => s!=null && s.nonEmpty)


  //TODO: helper to manually trigger emissions of Rx Observables

  def obsOnline : Observable[Boolean] = ???
  def obsToken : Observable[Option[String]] = ???
  def obsTokenWithBearer : Observable[Option[String]] = obsToken.map(s => s.map(k => "Bearer "+k))
  def obsTokenParsed : Observable[Option[OpenIDToken.IdentityToken]] = obsTokenWithBearer.map(o => o.flatMap(s => Try(OpenIDToken.base64EncodedTokenWithBearerHeader(s)).toOption))
  def obsEmail : Observable[Option[Email]] = obsTokenParsed.map(_.map(id => Email(id.email)))
  def obsAggregates : Observable[Seq[Aggregate]] = ???
  def obsCommandResults : Observable[Seq[Try[Event[Aggregate]]]] = ???

}
