package org.nephtys.cqrssync.services

import scala.concurrent.Future

/**
  * Created by nephtys on 11/25/16.
  */
trait HttpServiceComponent {

  def httpService : HttpService

  trait HttpService {

    import org.nephtys.cqrssync.datastructures.HttpServiceReturnValues._
    def getFrom(url : String, etag : Long = 0) : Future[CachedReturnValue]

    def postTo(url : String, content : String) : Future[ReturnValue]

  }


}
