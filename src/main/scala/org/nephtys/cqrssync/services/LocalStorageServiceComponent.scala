package org.nephtys.cqrssync.services

/**
  * Created by nephtys on 11/25/16.
  */
trait LocalStorageServiceComponent {

  def localStorageService : LocalStorageService

  val TokenLocalStorageKey : String = "OpenIDConnectGoogleIDToken"

  trait LocalStorageService {

    def readFrom(key : String) : String

    def writeTo(key : String, value : String) : Unit

  }
}
