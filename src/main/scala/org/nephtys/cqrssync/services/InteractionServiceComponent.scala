package org.nephtys.cqrssync.services

/**
  * Created by nephtys on 11/25/16.
  */
trait InteractionServiceComponent {

  def interactionService : InteractionService

  trait InteractionService {

    def confirm(text : String) : Boolean

    def alert(text : String) : Unit
  }
}
