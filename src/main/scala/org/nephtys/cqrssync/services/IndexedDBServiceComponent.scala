package org.nephtys.cqrssync.services

import upickle.default._

import org.nephtys.cqrssync.datastructures.{AggregateWithID, DatabaseConfiguration, IDable}

import scala.concurrent.Future

/**
  * Created by nephtys on 11/25/16.
  */
trait IndexedDBServiceComponent {

  def indexedDBService : IndexedDBService

  def databaseConfig : DatabaseConfiguration

  trait IndexedDBService {

    def setFromToDB[T <: IDable](seq : Seq[T])(implicit writer : Writer[T]) : Future[Seq[T]]

    def clearDB[T <: IDable]()(implicit writer : Writer[T]): Future[Seq[T]] = setFromToDB(Seq.empty[T])

    def readAllFromDB[T <: IDable]()(implicit reader : Reader[T])  : Future[Seq[T]]

    def appendToDB[T <: IDable](appendedElements : Seq[T])(implicit writer : Writer[T])  : Future[Seq[T]]

  }
}
