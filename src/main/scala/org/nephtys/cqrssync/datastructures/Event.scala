package org.nephtys.cqrssync.datastructures

import java.util.UUID

/**
  * Created by nephtys on 11/25/16.
  */
trait Event[Aggregate <: AggregateWithID] {

  def commit(aggregates : Map[UUID, Aggregate]) : Map[UUID, AggregateWithID]

}
