package org.nephtys.cqrssync.datastructures

import scala.util.Try

/**
  * Created by nephtys on 11/25/16.
  */
final case class Failable[T](success: Option[T], failure : Option[String]) {
  def isSuccess: Boolean = failure.isEmpty && success.isDefined
  def isFailure: Boolean = !isSuccess
  def asTry : Try[T] = Try(success.getOrElse(throw new Exception(failure.get)))
}