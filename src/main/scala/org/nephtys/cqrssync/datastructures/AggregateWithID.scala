package org.nephtys.cqrssync.datastructures

import java.util.UUID

/**
  * Created by nephtys on 11/25/16.
  */
trait AggregateWithID extends IDable{
  def uuid : UUID

  override def id: String = uuid.toString
}
