package org.nephtys.cqrssync.datastructures

/**
  * Created by nephtys on 11/25/16.
  */
final case class CommandContainer[T <: AggregateWithID, Eventable <: Event[T]](uniqueAscendingID : Long, command : Command[T, Eventable]) extends IDable {
//no two queues or anything, so we do not need that information. just the ID to guarantee ordering
  override def id: String = uniqueAscendingID.toString
}
