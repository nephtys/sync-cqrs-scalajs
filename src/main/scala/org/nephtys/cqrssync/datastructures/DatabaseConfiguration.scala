package org.nephtys.cqrssync.datastructures

/**
  * Created by nephtys on 11/25/16.
  */
case class DatabaseConfiguration(databaseName : String, objectStoreAggregates : String, objectStoreCommandContainers : String, databaseVersion : Long = 1) {

}
