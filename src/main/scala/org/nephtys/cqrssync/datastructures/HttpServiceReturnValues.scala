package org.nephtys.cqrssync.datastructures

/**
  * Created by nephtys on 11/25/16.
  */
object HttpServiceReturnValues {

  final case class CachedReturnValue(body : String, eTag : Long, status : Int)
  final case class ReturnValue(body : String, status : Int)


  val SUCCESS : Int = 200
  val UNMODIFIED : Int = 304
  val UNAUTHORIZED : Int = 401
}
