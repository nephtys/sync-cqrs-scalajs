package org.nephtys.cqrssync.datastructures

import java.util.UUID

import scala.util.Try

/**
  * Created by nephtys on 11/25/16.
  */
trait Command[Aggregate <: AggregateWithID, Eventatable <: Event[Aggregate]] {

  def uuid : UUID

  def validate(aggregates : Map[UUID, Aggregate]) : Try[Eventatable]
}