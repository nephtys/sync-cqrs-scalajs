package org.nephtys.cqrssync.datastructures

import upickle.default._
import com.github.marklister.base64.Base64._

/**
  * Created by nephtys on 11/25/16.
  */
object OpenIDToken {
  case class IdentityToken(
                            iss: String,
                            iat: Double,
                            exp: Double,
                            at_hash: String,
                            aud: String,
                            sub: String,
                            email_verified: Boolean,
                            azp: String,
                            email: String
                          )

  def base64EncodedTokenWithBearerHeader(token : String) : IdentityToken = {
    read[IdentityToken](decodeFromBase64(removeHeader(token).trim.split('.')(1)))
  }

  def decodeFromBase64(value : String) : String = new String(value.toByteArray(base64Url), "utf-8")
  def removeHeader(headerValue : String) : String = headerValue.split("Bearer ").last.trim
}
