import java.util.UUID

import org.nephtys.cqrssync.datastructures.{Command, Event}

import scala.util.Try

/**
  * Created by nephtys on 11/25/16.
  */
object PowerAggregateProtocol {
  sealed trait PowerEvent extends Event[PowerAggregate]

  sealed trait PowerCommand extends Command[PowerAggregate, PowerEvent]


  final case class PowerCreationCommand(uuid : UUID) extends PowerCommand {
    override def validate(aggregates: Map[UUID, PowerAggregate]): Try[Event[PowerAggregate]] = Try(PowerCreationEvent(uuid))
  }
  final case class PowerCreationEvent(uuid : UUID) extends PowerEvent {
    override def commit(aggregates: Map[UUID, PowerAggregate]): Map[UUID, PowerAggregate] = {
      aggregates.+((uuid, PowerAggregate(uuid, "")))
    }
  }

  final case class PowerRenamingCommand(uuid : UUID, name : String) extends PowerCommand {
    override def validate(aggregates: Map[UUID, PowerAggregate]): Try[Event[PowerAggregate]] = Try(PowerRenameEvent(uuid, name))
  }
  final case class PowerRenameEvent(uuid : UUID, name : String) extends PowerEvent {
    override def commit(aggregates: Map[UUID, PowerAggregate]): Map[UUID, PowerAggregate] = {
      aggregates.+((uuid, PowerAggregate(uuid, name)))
    }
  }

  final case class PowerDeletionCommand(uuid : UUID) extends PowerCommand {
    override def validate(aggregates: Map[UUID, PowerAggregate]): Try[Event[PowerAggregate]] = Try(PowerDeletionEvent(uuid))
  }
  final case class PowerDeletionEvent(uuid : UUID) extends PowerEvent {
    override def commit(aggregates: Map[UUID, PowerAggregate]): Map[UUID, PowerAggregate] = {
      aggregates.-(uuid)
    }
  }
}
