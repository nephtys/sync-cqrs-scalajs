import java.util.UUID

import PowerAggregateProtocol.PowerCreationCommand
import StateFactory.User
import org.nephtys.cqrssync.SyncModule
import org.nephtys.cqrssync.datastructures.{Command, DatabaseConfiguration, Email, Event}
import org.nephtys.cqrssync.datastructures.HttpServiceReturnValues.{CachedReturnValue, ReturnValue}
import org.scalatest._
import upickle.default._

import scala.concurrent.Future
import scala.util.Try

/**
  * Created by nephtys on 11/25/16.
  */
class B2Spec extends AsyncFlatSpec with Matchers {

  private val uuid1 = UUID.randomUUID()
  private val uuid2 = UUID.randomUUID()

  private val results = Seq(Seq(PowerAggregate(uuid1, "somename")), Seq(PowerAggregate(uuid1, "somename"), PowerAggregate(uuid2, "")), Seq(PowerAggregate(uuid1, "somename"), PowerAggregate(uuid2, "")))
  private val etags : Seq[Long] = Seq(1, 2, 2)

  "Logged in user being online" should "get data on first trigger" in {
    val sm : SyncModule[PowerAggregate, PowerAggregateProtocol.PowerCommand, PowerAggregateProtocol.PowerEvent] = new SyncModule[PowerAggregate, PowerAggregateProtocol.PowerCommand, PowerAggregateProtocol.PowerEvent] with InteractionServiceMock with HttpServiceMock with IndexedDBServiceMock with LocalStorageMock {
      override var httpGetAnswers: Seq[CachedReturnValue] = results.zip(etags).map(a => CachedReturnValue( write(a._1),a._2, 200))
      override var httpPostAnswers: Seq[ReturnValue] = Seq.empty[ReturnValue]
      override val user: User = StateFactory.user.loggedIn.online

      override def databaseConfig: DatabaseConfiguration = StateFactory.dbConfig
    }
    sm.triggerTimer().map(a => assert(a))
  }


  "With logged in user being online the system" should "immediately send to server and use etag caching" in {
    val sm : SyncModule[PowerAggregate, PowerAggregateProtocol.PowerCommand, PowerAggregateProtocol.PowerEvent] = new SyncModule[PowerAggregate, PowerAggregateProtocol.PowerCommand, PowerAggregateProtocol.PowerEvent] with InteractionServiceMock with HttpServiceMock with IndexedDBServiceMock with LocalStorageMock {
      override var httpGetAnswers: Seq[CachedReturnValue] = results.zip(etags).map(a => CachedReturnValue( write(a._1),a._2, 200))
      override var httpPostAnswers: Seq[ReturnValue] = Seq.empty[ReturnValue]
      override val user: User = StateFactory.user.loggedIn.online

      override def databaseConfig: DatabaseConfiguration = StateFactory.dbConfig
    }
    sm.triggerTimer().flatMap(unit => {
      val c = PowerCreationCommand(uuid2)
      sm.testCommand(c).flatMap[(Boolean, Try[Event[PowerAggregate]])]((firstBoolean : Boolean) => {
        sm.testAndApplyCommand(c).map(t => (firstBoolean, t) )
      }).flatMap( (com : (Boolean, Try[Event[PowerAggregate]])) => {
        sm.triggerTimer().map(e => com)
      })
    }).map(a => assert(a._1 && a._2.isSuccess))

  }
}
