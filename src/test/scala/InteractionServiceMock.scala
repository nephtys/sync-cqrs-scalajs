import org.nephtys.cqrssync.services.InteractionServiceComponent

/**
  * Created by nephtys on 11/25/16.
  */
trait InteractionServiceMock extends InteractionServiceComponent {
  def interactionAnswers : List[Boolean] = List(true, false, true)

  private var internalAnswers : List[Boolean] = interactionAnswers

  var textsShown: List[String] = List[String]()

  override val interactionService: InteractionService = new InteractionService {
    override def confirm(text: String) : Boolean = {
      textsShown = text::textsShown
      val b = internalAnswers.head
      internalAnswers = internalAnswers.tail
      b
    }

    override def alert(text: String) : Unit = textsShown = text::textsShown
  }
}
