import org.nephtys.cqrssync.services.LocalStorageServiceComponent

import scala.collection.mutable

/**
  * Created by nephtys on 11/25/16.
  */
trait LocalStorageMock extends LocalStorageServiceComponent {

  val user : StateFactory.User

  private val localStorageMockMap : mutable.Map[String, String] = mutable.Map.empty[String, String]

  override val localStorageService: LocalStorageService = new LocalStorageService {
    override def writeTo(key: String, value : String): Unit = localStorageMockMap.put(key, value)

    override def readFrom(key: String): String = localStorageMockMap.getOrElse(key, "")
  }

  def setLocalStorage(key : String, value : String) : Unit = localStorageMockMap.put(key, value)
}
