import java.util.concurrent.atomic.AtomicInteger

import org.nephtys.cqrssync.services.HttpServiceComponent
import org.nephtys.cqrssync.datastructures.HttpServiceReturnValues.{CachedReturnValue, ReturnValue}

import scala.concurrent.Future

/**
  * Created by nephtys on 11/25/16.
  */
trait HttpServiceMock extends HttpServiceComponent {

  var httpGetAnswers : Seq[CachedReturnValue]

  var httpPostAnswers : Seq[ReturnValue]

  val getCounter : AtomicInteger = new AtomicInteger(0)
  val postCounter : AtomicInteger = new AtomicInteger(0)


  override val httpService: HttpService = new HttpService {
    override def getFrom(url: String, etag: Long): Future[CachedReturnValue] = Future.successful(httpGetAnswers(getCounter.getAndIncrement()))

    override def postTo(url: String, content: String): Future[ReturnValue] = Future.successful(httpPostAnswers(postCounter.getAndIncrement()))
  }
}
