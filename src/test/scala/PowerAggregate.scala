import java.util.UUID

import org.nephtys.cqrssync.datastructures.{AggregateWithID, Command, Event}

import scala.util.Try

/**
  * Created by nephtys on 11/25/16.
  */
final case class PowerAggregate(uuid : UUID, name : String) extends AggregateWithID{
}


