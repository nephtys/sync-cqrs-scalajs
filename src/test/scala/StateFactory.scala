import org.nephtys.cqrssync.datastructures.DatabaseConfiguration

/**
  * Created by nephtys on 11/25/16.
  */
object StateFactory {

  case class User(loggedInAs: Option[String], isOnline: Boolean) {
    def loggedIn: User = loggedInValid

    def loggedInInvalid: User = copy(loggedInAs = tokens.invalid)

    def loggedInNot: User = copy(loggedInAs = tokens.unset)

    def loggedInValid: User = copy(loggedInAs = tokens.valid)

    def loggedInTooOld: User = copy(loggedInAs = tokens.tooOld)

    def online: User = copy(isOnline = true)

    def offline: User = copy(isOnline = false)

  }

  def user: User = User(tokens.unset, isOnline = false)



  object tokens {
    val invalid: Option[String] = Some("invalidtoken")
    val valid: Option[String] = Some("validtoken")
    val tooOld: Option[String] = Some("tokenIsTooOld")
    val unset: Option[String] = None
  }

  def dbConfig = DatabaseConfiguration("TestDatabaseName", "AggregateObjectStore", "CommandObjectStore", 2)
}
