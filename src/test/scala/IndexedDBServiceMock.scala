import org.nephtys.cqrssync.datastructures.{AggregateWithID, IDable}
import org.nephtys.cqrssync.services.IndexedDBServiceComponent

import scala.concurrent.Future

/**
  * Created by nephtys on 11/25/16.
  */
trait IndexedDBServiceMock extends IndexedDBServiceComponent {

  override val indexedDBService: IndexedDBService = new IndexedDBService {
    override def setFromToDB[T <: IDable](seq: Seq[T])(implicit writer: _root_.upickle.default.Writer[T]): Future[Seq[T]] = ???

    override def readAllFromDB[T <: IDable]()(implicit reader: _root_.upickle.default.Reader[T]): Future[Seq[T]] = ???

    override def appendToDB[T <: IDable](appendedElements: Seq[T])(implicit writer: _root_.upickle.default.Writer[T]): Future[Seq[T]] = ???
  }
}
