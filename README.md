This is a simple scala.js sided library to enable hybrid CQRS: commands are created locally (even offline!) and are applied to the local copy of the aggregate (persisted with the help of IndexedDB) and afterwards send to the server. 

The server still has the last word which kind of CQRS Command is allowed for the aggregate, but this way we get an immediate response on the client side AND we can persist commands even if our Client or our Server are currently offline.

This is the main application: allowing CQRS-based web applications to run offline with full functionality, synchronizing during the next online state.

It is very inefficiently implemented and very limited in its functionality. It is also not using any shims, so current browsers are a requirement.

I did not test it against node.js and it is unlikely to work on that platform (and kind of useless)




Currently using:

* upickle for seralization
* scalatest for testing
* RxScalaJs for supplying a very generic Rx interface to bind to the calling ScalaJS code
* scalajs-dom to directly access DOM features like IndexedDB or LocalStorage