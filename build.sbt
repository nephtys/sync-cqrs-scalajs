enablePlugins(ScalaJSPlugin)

name := "sync-cqrs-scalajs"

version := "1.0"

scalacOptions ++= Seq("-unchecked", "-deprecation")

scalaVersion := "2.12.0"

libraryDependencies += "com.lihaoyi" %%% "upickle" % "0.4.4"
libraryDependencies += "org.scalatest" %%% "scalatest" % "3.0.0" % "test"
libraryDependencies += "com.github.lukajcb" %%% "rxscala-js" % "0.9.2"
libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.1"
libraryDependencies += "com.github.marklister" %%% "base64" % "0.2.3"

jsDependencies += "org.webjars.npm" % "rxjs" % "5.0.0-rc.4" / "bundles/Rx.min.js" commonJSName "Rx"
